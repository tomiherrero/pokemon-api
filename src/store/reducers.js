import {combineReducers} from 'redux';

import {reducers} from '@state';

const createRootReducer = () => combineReducers({
    ...reducers
});

export default createRootReducer;
